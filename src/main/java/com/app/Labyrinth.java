package com.app;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Vector;

public class Labyrinth {

    public static final char START_SIGN = '3';
    public static final char END_SIGN = '2';
    public static final char WALL_SIGN = '0';
    public static final char ROAD_SIGN = '1';

    public static final char PATH_START = 'x';
    public static final char PATH_END = 'E';

    private StringBuilder[] matrix;
    private int rows, cols;

    private Position startPosition;
    public Graph<Position, Integer, Integer> graph;

    public Labyrinth(String inputFile) {
        readStringArray(inputFile);
        findAndSetStartPosition(START_SIGN);
        mapToGraph();
    }

    public void solve() {
        drawEscapePaths(graph.breadthDo(startPosition, currentNode -> null));
    }

    public StringBuilder[] getMatrix() {
        return matrix;
    }

    private void allocateMatrix(int rows, int cols) {
        setRows(rows);
        setCols(cols);
        this.matrix = new StringBuilder[rows];
    }

    public Position getStartPosition() {
        return startPosition;
    }

    private void setStartPosition(Position startPosition) {
        Test.assertThis(startPosition != null, "Null start position.");
        this.startPosition = startPosition;
    }

    private void findAndSetStartPosition(char startMarker) {
        for (int i = 0; i < getRows(); ++i)
            for (int j = 0; j < getCols(); ++j)
                if (matrix[i].charAt(j) == startMarker)
                    setStartPosition(new Position(i, j));
    }

    private void readStringArray(String inputFilePath) {
        File inputFile = new File(inputFilePath);
        /*
         * PrintWriter.output = new PrintWriter(file);
         * output.println("blabla");
         * output.close();
         */

        try {
            Scanner in = new Scanner(inputFile);
            allocateMatrix(in.nextInt(), in.nextInt());

            in.nextLine(); // skip '\n'
            for (int i = 0; i < getRows(); ++i) {
                matrix[i] = new StringBuilder(in.nextLine());

                // check
                Test.assertThis(matrix[i].length() == getCols(), "Input is ill-formed.");
            }

            in.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found! " + e);
            throw new AssertionError();
        }
    }


    private void mapToGraph() {
        graph = new Graph<>();

        for (int i = 0; i < getRows(); ++i) {
            for (int j = 0; j < getCols(); ++j) {
                Position currentPos = new Position(i, j);

                // Skip walls
                if (matrix[i].charAt(j) == WALL_SIGN) continue;

                if (!graph.hasNode(currentPos)) {
                    graph.insertNode(currentPos, Character.getNumericValue(matrix[i].charAt(j)));
                }

                // check North
                if (i - 1 >= 0 && matrix[i - 1].charAt(j) != WALL_SIGN) {
                    graph.insertEdgeBi(currentPos, new Position(i - 1, j), 1);
                }

                // check East
                if (j - 1 >= 0 && matrix[i].charAt(j - 1) != WALL_SIGN) {
                    graph.insertEdgeBi(currentPos, new Position(i, j - 1), 1);
                }
            }
        }
    }

    public void printMatrix() {
        for (int i = 0; i < getRows(); ++i) {
            for (int j = 0; j < getCols(); ++j) {
                switch (matrix[i].charAt(j)) {
                    case WALL_SIGN:
                        System.out.print('-');
                        break;
                    case ROAD_SIGN:
                        System.out.print('*');
                        break;
                    default:
                        System.out.print(matrix[i].charAt(j));
                }
            }
            System.out.print('\n');
        }
    }

    public void drawMatrix(Graphics g, int topLeft, int squareSize) {
        int horizontalPos, verticalPos;

        verticalPos = topLeft;
        for (int i = 0; i < getRows(); ++i) {
            horizontalPos = topLeft;

            for (int j = 0; j < getCols(); ++j) {
                Color color;
                switch(matrix[i].charAt(j)) {
                    case WALL_SIGN:
                        color = Color.DARK_GRAY;
                        break;
                    case ROAD_SIGN:
                        color = Color.WHITE;
                        break;
                    case PATH_START:
                        color = Color.BLUE;
                        break;
                    case PATH_END:
                        color = Color.GREEN;
                        break;
                    default:
                        color = Color.RED;
                }
                drawSquare(g, new Point(horizontalPos, verticalPos), color, squareSize);
                horizontalPos += squareSize + 1;
            }
            verticalPos += squareSize + 1;
        }
    }

    private void drawSquare(Graphics g, Point p, Color color, int size) {
        g.setColor(color);
        g.fillRect(p.x, p.y, size, size); // inside
        g.setColor(Color.BLACK);
        g.drawRect(p.x, p.y, size, size); // border
    }

    private void drawPathToMatrix(Vector<Position> path, char markValue) {
        Test.assertThis(path != null, "Null path.");
        for (Position p : path)
            matrix[p.getKey()].setCharAt(p.getValue(), markValue);
    }

    private void drawEscapePaths(HashMap<Position, Position> visited) {
        String makers = "abcdef";
        int makerIndex = 0;

        System.out.println("\nvisited:");

        for (Vector<Position> path : graph.getPathsFromVisited(visited, 2)) {
            drawPathToMatrix(path, makers.charAt(makerIndex++));

            Position last = path.lastElement();
            matrix[last.getKey()].setCharAt(last.getValue(), PATH_END);

            // [Debug]
            for(Position current : path) {
                System.out.print(graph.nodeData(current) + " ");
            }
            System.out.println('\n');
        }
        matrix[getStartPosition().getKey()].setCharAt(getStartPosition().getValue(), PATH_START);
    }


    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        Test.assertThis(rows > 1, "Invalid number of rows.");
        this.rows = rows;
    }

    public int getCols() {
        return cols;
    }

    public void setCols(int cols) {
        Test.assertThis(cols > 1, "Invalid number of cols.");
        this.cols = cols;
    }
}
