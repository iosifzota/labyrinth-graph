package com.app;

import java.util.*;
import java.util.function.*;
import java.util.concurrent.Callable;

/**
 * Digraph (directed & undired)
 *
 * Done: node() -> optional
 * TODO: edge() -> optional
 * Done: neighborsOf() -> optional
 * Done: edgesOfNode() -> optional
 * EDIT: Removed implenents Comparable<>; meh
 * @param <NodeKey>
 * @param <NodeData>
 * @param <EdgeData>
 */
public class Graph<NodeKey, NodeData, EdgeData> {
        private HashMap<NodeKey, NodeData> nodes;
        private HashMap<NodeKey, HashMap<NodeKey, EdgeData>> edges;  // key -> (key -> edge)

        public Graph() {
                nodes = new HashMap<>();
                edges = new HashMap<>();
        }

        // No edgesCardinal because it's way to expensive.
        public int nodesCardinal() {
                return nodes.size();
        }

        public boolean isEmpty() {
                return nodes.isEmpty();
        }

        public boolean hasNode(NodeKey k) {
                return nodes.containsKey(k);
        }

        public Set<NodeKey> nodeKeysSet() {
                return nodes.keySet();
        }

        public void insertNode(NodeKey k, NodeData d) {
                nodes.putIfAbsent(k, d);
        }

        public NodeData nodeData(NodeKey k) {
                return nodes.get(k);
        }

        // rm all connection to node
        public void cutoffNode(NodeKey k) {
                HashMap<NodeKey, EdgeData> endToEdge = edges.get(k);

                if (endToEdge == null) {
                        return;
                }

                // rm edges ... -> k
                for (NodeKey currentEndKey : endToEdge.keySet()) {
                        removeEdgeOneWay(currentEndKey, k);
                }

                // rm edges k -> ...
                edges.remove(k);
        }

        // rm node & all the edges it belongs to
        public void removeNode(NodeKey k) {
                nodes.remove(k);
                cutoffNode(k);
        }

        public boolean hasEdge(NodeKey begin, NodeKey end) {
                HashMap<NodeKey,EdgeData> endToEdge = edges.get(begin);
                // make sure not to dref null
                return (endToEdge != null) && endToEdge.containsKey(end);
        }

        // ins (begin, end)
        public void insertEdgeOneWay(NodeKey begin, NodeKey end, EdgeData edgeData) {
                // insert edge iff *begin*, *end* are in the *nodes* set.
                if (!hasNode(begin) || !hasNode(end))
                        return;

                edges.putIfAbsent(begin, new HashMap<>());
                edges.get(begin).putIfAbsent(end, edgeData);
        }

        // ins (begin, end) & (end, begin)
        public void insertEdgeBi(NodeKey begin, NodeKey end, EdgeData edgeData) {
                insertEdgeOneWay(begin, end, edgeData);
                insertEdgeOneWay(end, begin, edgeData);
        }

        // rm (begin, end)
        public void removeEdgeOneWay(NodeKey begin, NodeKey end) {
                HashMap<NodeKey, EdgeData> endToEdge = edges.get(begin);

                if (endToEdge != null) {
                        endToEdge.remove(end);

                        // delete empty entry
                        if (endToEdge.isEmpty()) {
                                edges.remove(begin);
                        }
                }
        }

        // rm (begin, end) and (end, begin)
        public void removeEdgeBi(NodeKey begin, NodeKey end) {
                removeEdgeOneWay(begin, end);
                removeEdgeOneWay(end, begin);
        }

        public void transformToUndiredcted(EdgeData defaultValue) {
                HashMap<NodeKey, HashMap<NodeKey, EdgeData>> edgesTemp;  // key -> (key -> edge)
                edgesTemp = edges;
                edges = new HashMap<>();
                for (NodeKey begin : edgesTemp.keySet()) {
                        HashMap<NodeKey, EdgeData> endToEdge = edgesTemp.get(begin);
                        if (endToEdge == null) throw new AssertionError("[Debug]");
                        for (NodeKey end : endToEdge.keySet()) {
                                insertEdgeBi(end, begin, defaultValue);
                        }
                }
        }

        public void transpose(EdgeData defaultValue) {
                HashMap<NodeKey, HashMap<NodeKey, EdgeData>> edgesTemp;  // key -> (key -> edge)
                edgesTemp = edges;
                edges = new HashMap<>();
                for (NodeKey begin : edgesTemp.keySet()) {
                        HashMap<NodeKey, EdgeData> endToEdge = edgesTemp.get(begin);
                        if (endToEdge == null) throw new AssertionError("[Debug]");
                        for (NodeKey end : endToEdge.keySet()) {
                                insertEdgeOneWay(end, begin, defaultValue);
                        }
                }
        }

        public Set<NodeKey> neighborsOf(NodeKey k) {
                HashMap<NodeKey,EdgeData> endToEdge = edges.get(k);

                if (endToEdge != null) {
                        return endToEdge.keySet();
                }
                return null;
        }

        public boolean areNeighborsBi(NodeKey k, NodeKey anotherKey) {
                return hasEdge(k, anotherKey) || hasEdge(anotherKey, k);
        }

        @Override
        public String toString() {
                StringBuilder display = new StringBuilder("\nCheck neighbors of each node ...\n");

                for (NodeKey currentNode : nodes.keySet()) {
                        display.append("Neighbors of ").append(currentNode).append(":\n");

                        Set<NodeKey> neighborsSet = neighborsOf(currentNode);
                        if (neighborsSet != null) {
                                for (NodeKey neighbor : neighborsSet) {
                                        display.append(neighbor).append("\n");
                                }
                        } else {
                                display.append("No neighbors.\n");
                        }

                        display.append('\n');
                }
                return display.toString();
        }

        public HashMap<NodeKey, NodeKey> breadthDo(NodeKey source, Function<NodeKey, Void> action) {
                Queue<NodeKey> fringe = new LinkedList<>();
                HashMap<NodeKey, NodeKey> visited = new HashMap<>();


                if (!hasNode(source))
                        return null;

                fringe.add(source);
                visited.put(source, source);

                while (!fringe.isEmpty()) {
                        NodeKey currentNode = fringe.poll();

                        // do
                        action.apply(currentNode);

                        // add neighbors in queue (iff !visited)
                        Set<NodeKey> neighbors = neighborsOf(currentNode);
                        if (neighbors != null) {
                                for (NodeKey neighbor : neighbors) {
                                        if (!visited.containsKey(neighbor)) {
                                                visited.put(neighbor, currentNode);
                                                fringe.add(neighbor);
                                        }
                                }
                        }
                }
                return visited;
        }

        private class NodeItr {
                boolean analyzing;
                Iterator<NodeKey> neighborsItr;

                NodeItr() {
                        analyzing = false;
                        neighborsItr = null;
                }

                NodeItr(Iterator itr) {
                        this.analyzing = false;
                        this.neighborsItr = itr;
                }

                void setItr(Iterator<NodeKey> itr) {
                        neighborsItr = itr;
                }

                boolean hasNext() {
                        if (neighborsItr == null) return false;
                        return neighborsItr.hasNext();
                }

                NodeKey next() {
                        if (hasNext())
                                return neighborsItr.next();
                        return null;
                }

                boolean isAnalyzing() {
                        return analyzing;
                }
                void setAnalyzing() {
                        analyzing = true;
                }
        }

        // TODO with breadthDo only with BiConsumer
        public void forEachDF(NodeKey source, Consumer<NodeKey> initial, Consumer<NodeKey> recurr, Consumer<Void> after) {
                Stack<NodeKey> fringe = new Stack<>();
                HashMap<NodeKey, NodeItr> visited = new HashMap<>();

                flagSD(source, visited, fringe);
                initial.accept(source);

                while (!fringe.isEmpty()) {
                        NodeKey currentNode = fringe.peek();
                        NodeItr currentMeta = visited.get(currentNode);

                        if (currentMeta.hasNext()) {
                                NodeKey neighbor = currentMeta.next();
                                if (neighbor == null) throw new AssertionError("[Debug] hasNext() was called! *** next() should not return null.");

                                // attempt visting neighbor
                                if (!visited.containsKey(neighbor)) {
                                        if (currentMeta.isAnalyzing()) { // if recurrs again
                                                initial.accept(currentNode);
                                        } else {
                                                currentMeta.setAnalyzing();
                                        }

                                        flagSD(neighbor, visited, fringe); //meh
                                        recurr.accept(neighbor);
                                }
                        } else { // analyzed
                                after.accept(null);
                                fringe.pop();
                        }
                }
        }

        private void flagSD(NodeKey key,
                            HashMap<NodeKey, NodeItr> visited, Stack<NodeKey> fringe
                ) {
                Set<NodeKey> neighbors = neighborsOf(key);
                Iterator<NodeKey> neighborsItr = null;
                // Init neighborsItr
                if (neighbors != null) {
                        neighborsItr = neighbors.iterator();
                }
                visited.put(key, new NodeItr(neighborsItr));
                fringe.add(key);
        }

        public NodeKey firstNode() {
                return nodes.keySet().iterator().next();
        }

        public void forEachNode(BiConsumer<NodeKey, NodeData> action) {
                nodes.forEach((k, data) -> action.accept(k, data));
        }

        public enum CiclesPredicate {
                Allowed,
                None
        }

        private class NodeMeta {
                int t1, t2;
                NodeKey visitor;
                Iterator<NodeKey> neighborsItr;

                static final int unset = -1;

                NodeMeta() {
                        visitor = null;
                        t1 = unset;
                        t2 = unset;
                        neighborsItr = null;
                }

                NodeMeta(NodeKey key, int t1, Iterator itr) {
                        if (key == null) throw new AssertionError("Visitor cannot be null!");
                        visitor = key;
                        this.t1 = t1;
                        this.t2 = unset;
                        this.neighborsItr = itr;
                }

                void set1(int i) {
                        if (i == unset) throw new AssertionError("Cannot set to `unset'!");
                        t1 = i;
                }

                void setItr(Iterator<NodeKey> itr) {
                        neighborsItr = itr;
                }

                boolean hasNext() {
                        if (neighborsItr == null) return false;
                        return neighborsItr.hasNext();
                }

                NodeKey next() {
                        if (hasNext())
                                return neighborsItr.next();
                        return null;
                }

                void set2(int i) {
                        if (i == unset) throw new AssertionError("Cannot set to `unset'!");
                        t2 = i;
                }

                void setVisitor(NodeKey key) { // keys are unique
                        visitor = key;
                }

                int get1() {
                        if (t1 == unset) throw new AssertionError("Cannot get `unset'!");
                        return t1;
                }

                int get2() {
                        if (t2 == unset) throw new AssertionError("Cannot get `unset'!");
                        return t2;
                }

                NodeKey getVisitor(NodeKey key) {
                        if (visitor == null) throw new AssertionError("Cannot get null visitor!");
                        return visitor;
                }

                boolean analyzed() {
                        return visited() && t2 != unset;
                }

                boolean visited() {
                        return visitor != null && t1 != unset;
                }

                boolean onlyVisited() {
                        return visited() && !analyzed();
                }
        }

        // MAYBE:
        // private class TotalDepthDoState {
        //         public NodeKey key, parent;
        //         public int t1;
        //         public HashMap<NodeKey, NodeMeta> visited;
        //         public HashSet<NodeKey> untouched;
        //         public Stack<NodeKey> fringe;

        //         TotalDepthDoState(NodeKey key, NodeKey parent, int t1,
        //                           HashMap<NodeKey, NodeMeta> visited, HashSet<NodeKey> untouched,
        //                           Stack<NodeKey> fringe) {
        //                 this.key = key;
        //                 this.t1 = t1;
        //                 this.visited = visited;
        //                 this.untouched = untouched;
        //                 this.fringe = fringe;
        //         }
        // }

        private void sflag(NodeKey key, NodeKey parent, int t1,
                           HashMap<NodeKey, NodeMeta> visited, HashSet<NodeKey> untouched,
                           Stack<NodeKey> fringe
                ) {
                Set<NodeKey> neighbors = neighborsOf(key);
                Iterator<NodeKey> neighborsItr = null;
                // Init neighborsItr
                if (neighbors != null) {
                        neighborsItr = neighbors.iterator();
                }
                visited.put(key, new NodeMeta(parent, t1, neighborsItr));
                untouched.remove(key);
                fringe.add(key);
        }

        public List<NodeKey> topologicSort(NodeKey source) {
                List<NodeKey> res = new ArrayList<>();
                totalDepthDo(source,
                       key -> null,
                       key -> { res.add(key); return null; },
                       Graph.CiclesPredicate.None);
                return res;
        }

        public HashMap<NodeKey, NodeMeta> totalDepthDo(NodeKey source,
                                                 Function<NodeKey, Void> visitedDo,
                                                 Function<NodeKey, Void> analyzedDo,
                                                 CiclesPredicate ciclesAllowed
                ) {
                if (!hasNode(source))
                        return null;

                Stack<NodeKey> fringe = new Stack<>();
                HashMap<NodeKey, NodeMeta> visited = new HashMap<>();
                HashSet<NodeKey> untouched = new HashSet<>();
                int t = 0;

                for (NodeKey key : nodes.keySet()) {
                        untouched.add(key);
                }

                sflag(source, source, t++, visited, untouched, fringe); //meh
                visitedDo.apply(source);

                while(!untouched.isEmpty()) {
                        while (!fringe.isEmpty()) {
                                NodeKey currentNode = fringe.peek();

                                NodeMeta currentMeta = visited.get(currentNode);
                                if (currentMeta.hasNext()) {
                                        NodeKey neighbor = currentMeta.next();
                                        if (neighbor == null) throw new AssertionError("[Debug] hasNext() was called! *** next() should not return null.");

                                        // attempt visting neighbor
                                        if (!visited.containsKey(neighbor)) {
                                                sflag(neighbor, currentNode, t++, visited, untouched, fringe);
                                                visitedDo.apply(neighbor);
                                        } else if (ciclesAllowed == CiclesPredicate.None &&
                                                   visited.get(neighbor).onlyVisited()) { // meaning currentNode is still in the neighbor's subtree
                                                throw new AssertionError("A cicle was found! ciclesAllowed == None.");
                                        }
                                } else { // analyzed
                                        analyzedDo.apply(currentNode);
                                        visited.get(currentNode).set2(t++);
                                        fringe.pop();
                                }
                        }

                        // If all nodes have been visited
                        if (untouched.isEmpty()) {
                                return visited;
                        }

                        // start over
                        source = untouched.iterator().next();
                        sflag(source, source, t++, visited, untouched, fringe);
                        visitedDo.apply(source);

                        if (untouched.isEmpty()) {
                                NodeKey currentNode = fringe.pop();
                                analyzedDo.apply(currentNode);
                                visited.get(currentNode).set2(t++);
                        }
                }

                return visited;
        }

        private void flag(NodeKey key,
                          HashSet<NodeKey> visited, HashSet<NodeKey> untouched,
                          Stack<NodeKey> fringe, List<NodeKey> pack) {
                visited.add(key);
                untouched.remove(key);
                fringe.add(key);
                pack.add(key);
        }

        public List<List<NodeKey>> connectedComponents() {
                Stack<NodeKey> fringe = new Stack<>();
                HashSet<NodeKey> visited = new HashSet<>();
                HashSet<NodeKey> untouched = new HashSet<>();
                List<List<NodeKey>> allPacks = new ArrayList<>();

                for (NodeKey key : nodes.keySet()) {
                        untouched.add(key);
                }

                while(!untouched.isEmpty()) {
                        // start over
                        List<NodeKey> pack = new ArrayList<>(); // before flag!!
                        NodeKey source = untouched.iterator().next();
                        flag(source, visited, untouched, fringe, pack); //meh

                        while (!fringe.isEmpty()) {
                                NodeKey currentNode = fringe.pop();

                                // get neighbors
                                Set<NodeKey> neighbors = neighborsOf(currentNode);

                                if (neighbors != null) {
                                        // atempt visiting each neighbor
                                        for (NodeKey neighbor : neighbors) {
                                                if (!visited.contains(neighbor)) {
                                                        flag(neighbor, visited, untouched, fringe, pack); //meh
                                                }
                                        }
                                }
                        }

                        allPacks.add(pack);
                }


                return allPacks;

        }

        void strongF(NodeKey key, NodeKey source, Stack<NodeKey> fringe, List<NodeKey> pack, HashMap<NodeKey, NodeKey> packed, Stack<NodeKey> t2) {
                fringe.add(key);
                pack.add(key);
                packed.put(key, source);
                t2.pop();
        }

        public Graph<NodeKey, List<NodeKey>, Integer> strongConnectedComponents(EdgeData defaultValue) {
                final Graph<NodeKey, List<NodeKey>, Integer> allPacks = new Graph<>();

                if (nodes.isEmpty()) {
                        return allPacks;
                }

                HashMap<NodeKey, NodeMeta> visited =
                        totalDepthDo(nodes.keySet().iterator().next(),
                                     // (ign) -> { System.out.print(ign + " "); return null; },
                                     // (ign) -> { System.out.print(ign + " "); return null; },
                                     (ign) -> null,
                                     (ign) -> null,
                                     CiclesPredicate.Allowed);
                Stack<NodeKey> t2 = getSortedDescKeys(visited);

                // G^-1
                transpose(defaultValue);

                final HashMap<NodeKey, NodeKey> packed = new HashMap<>(); // book keeping
                HashMap<NodeKey, List<NodeKey>> connections = new HashMap<>();

                Stack<NodeKey> fringe = new Stack<>();
                while (!t2.isEmpty()) {
                        List<NodeKey> pack = new ArrayList<>(); // before flag!!

                        NodeKey source = t2.peek();
                        strongF(source, source, fringe, pack, packed, t2);

                        NodeMeta meta = visited.get(source);
                        int t1Source = meta.get1();
                        int t2Source = meta.get2();

                        while (!fringe.isEmpty()) {
                                NodeKey currentNode = fringe.pop();

                                // get neighbors
                                Set<NodeKey> neighbors = neighborsOf(currentNode);

                                if (neighbors != null)
                                        // For all unpacked nodes.
                                        for (NodeKey neighbor : neighbors) {
                                                if (!packed.containsKey(neighbor)) {
                                                        meta = visited.get(neighbor);
                                                        if (t1Source < meta.get1() && meta.get2() < t2Source) {
                                                                // neighbors belongs to source tree
                                                                strongF(neighbor, source, fringe, pack, packed, t2);
                                                        } else if (source != neighbor) { // traversal edge
                                                                if (!connections.containsKey(source)) {
                                                                        connections.put(source, new ArrayList<NodeKey>());
                                                                }
                                                                connections.get(source).add(neighbor);
                                                        }
                                                }  else if (source != neighbor) { // traversal edge
                                                        if (!connections.containsKey(source)) {
                                                                connections.put(source, new ArrayList<NodeKey>());
                                                        }
                                                        connections.get(source).add(neighbor);
                                                }
                                        }
                        }

                        allPacks.insertNode(source, pack);
                }

                connections.forEach((componentId, connectionsList) -> {
                                for (NodeKey adjKey : connectionsList) {
                                        allPacks.insertEdgeOneWay(packed.get(adjKey), componentId, 0);
                                }
                        });


                return allPacks;
        }

        public void treeDo(Consumer<NodeKey> initial, Consumer<NodeKey> recurr) {
                HashMap<NodeKey, NodeMeta> visited =
                        totalDepthDo(firstNode(),
                                     // (ign) -> { System.out.print(ign + " "); return null; },
                                     (ign) -> null,
                                     // (ign) -> { System.out.print(ign + " "); return null; },
                                     (ign) -> null,
                                     CiclesPredicate.None);
                final Stack<NodeKey> t2 = getSortedDescKeys(visited);

                while(!t2.isEmpty()) {
                        System.out.println("Tree:");
                        forEachDF(t2.peek(),
                                      (root) -> initial.accept(root),
                                      (nonRoot) -> recurr.accept(nonRoot),
                                      (ign) -> { t2.pop(); return; }
                                );
                        System.out.println("\n---");
                }
        }

        public Stack<NodeKey> getSortedDescKeys(HashMap<NodeKey, NodeMeta> visited) {
                final int size = 2 * visited.size();
                final Vector<NodeKey> t2 = new Vector<>(size);
                t2.setSize(size);

                visited.forEach(
                        (key, meta) -> {
                                t2.set(meta.get2(), key);
                        });


                Stack<NodeKey> ret = new Stack<>();
                for (int i = 0; i < t2.size(); ++i) {
                        if (t2.elementAt(i) != null) {
                                ret.add(t2.elementAt(i));
                        }
                }
                return ret;
        }

        public Vector<Vector<NodeKey>> getPathsFromVisited(HashMap<NodeKey, NodeKey> visited, NodeData endMarker) {

                Vector<Vector<NodeKey>> path = new Vector<>();

                visited.forEach((k, v) -> {
                                if (nodeData(k) == endMarker) {
                                        path.add(traceBackFrom(visited, k));
                                }
                        });

                return path;
        }

        public Vector<NodeKey> traceBackFrom(HashMap<NodeKey, NodeKey> visited, NodeKey current) {
                Vector<NodeKey> path = new Vector<>(1);

                path.add(0, current);

                while (current != visited.get(current)) {
                        path.add(0, visited.get(current));
                        current = visited.get(current);
                }

                return path;
        }
}
