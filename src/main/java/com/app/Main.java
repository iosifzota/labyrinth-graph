package com.app;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.util.*;
import java.io.*;

/**
 * Hello world!
 *
 */
public class Main
{
        private static final String inputFilePath = "lab.in";

        public static void main( String[] args ) {
                Test.testGraph();
                Labyrinth labyrinth = new Labyrinth(inputFilePath);

                // labyrinth.printMatrix();
                // labyrinth.solve();
                // labyrinth.printMatrix();
                /**
                 * Pornesc firul de executie grafic fie prin implementarea interfetei Runnable,
                 * fie printr-un obiect al clasei Thread.
                 */
                //SwingUtilities.invokeLater(() -> initUI(labyrinth));

                System.out.println("--------------------#");

                Graph<String, Integer, Integer> s = new Graph<>();
                s.insertNode("one", 1);
                s.insertNode("two", 2);
                s.insertNode("three", 3);
                s.insertNode("four", 4);
                s.insertNode("five", 5);
                s.insertNode("six", 6);
                s.insertNode("seven", 7);

                s.insertEdgeOneWay("one", "two", 1);
                s.insertEdgeOneWay("one", "four", 2);
                s.insertEdgeOneWay("two", "four", 3);
                s.insertEdgeOneWay("four", "three", 4);
                s.insertEdgeOneWay("four", "five", 5);
                s.insertEdgeOneWay("five", "seven", 6);
                s.insertEdgeOneWay("seven", "six", 7);

                List<String> sorted = s.topologicSort("one");
                System.out.println("Sorted: ");
                for (String key : sorted) {
                        System.out.print(key + " ");
                }
                System.out.println("");

                System.out.println("--------------------#");

                s.transformToUndiredcted(0);
                s.insertNode("solo", 10);
                List<List<String>> allPacks = s.connectedComponents();
                int nthPack = 0;
                for (List<String> pack : allPacks) {
                        System.out.println("Pack " + nthPack++ + ":");
                        for (String key : pack) {
                                System.out.print(key + " ");
                        }
                        System.out.println("");
                }

                System.out.println("\n--------------------#");

                Graph<String, Integer, Integer> ex = new Graph<>();

                // readStringGraph("strong.in", ex, 0, 0); TODO(fix)

                ex.insertNode("one", 1);
                ex.insertNode("two", 2);
                ex.insertNode("three", 3);
                ex.insertNode("four", 4);
                ex.insertNode("five", 5);
                ex.insertNode("six", 6);
                ex.insertNode("seven", 7);
                ex.insertNode("eight", 8);

                ex.insertEdgeOneWay("one", "two", 0);
                ex.insertEdgeOneWay("two", "six", 0);
                ex.insertEdgeOneWay("two", "three", 0);
                ex.insertEdgeOneWay("two", "five", 0);
                ex.insertEdgeBi("six", "seven", 0);
                ex.insertEdgeOneWay("seven", "eight", 0);
                ex.insertEdgeOneWay("three", "seven", 0);
                ex.insertEdgeBi("three", "four", 0);
                ex.insertEdgeOneWay("four", "eight", 0);
                ex.insertEdgeOneWay("five", "six", 0);
                ex.insertEdgeOneWay("five", "one", 0);

                final Graph<String, List<String>, Integer> packsGraph = ex.strongConnectedComponents(0);

                packsGraph.treeDo(
                        (root) -> {
                                System.out.print("\nComponent " + root + ": ");
                                for (String elem : packsGraph.nodeData(root)) {
                                        System.out.print(elem + " ");
                                }
                                System.out.print("  ");
                        },
                        (nonRoot) -> {
                                System.out.print("-> Component " + nonRoot + ": ");
                                for (String elem : packsGraph.nodeData(nonRoot)) {
                                        System.out.print(elem + " ");
                                }
                                System.out.print("  ");
                        });


        }

        private static <NodeData, EdgeData> void readStringGraph(String inputFileName, Graph<String, NodeData, EdgeData> g, NodeData defNodeVal, EdgeData defEdgeVal) {
                File inputFile = new File(inputFileName);
                /*
                 * PrintWriter.output = new PrintWriter(file);
                 * output.println("blabla");
                 * output.close();
                 */

                try {
                        Scanner in = new Scanner(inputFile);

                        while (in.hasNext("\\w*")) {
                                String begin = in.next("\\w*");
                                if (in.hasNext("\\w*")) {
                                        String end = in.next("\\w*");
                                        g.insertNode(begin, defNodeVal);
                                        g.insertNode(end, defNodeVal);
                                        g.insertEdgeOneWay(begin, end, defEdgeVal);
                                }
                        }
                        in.close();
                } catch (FileNotFoundException e) {
                        System.out.println("File not found! " + e);
                        throw new AssertionError();
                }
        }

    private static void initUI(Labyrinth labyrinth) {
        Test.assertThis(labyrinth != null, "initUI: Null input.");

        JFrame f = new JFrame("Labyrinth");
        // Close on exit
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Add canvas
        f.add(new Canvas(labyrinth));
        // Frame width & height
        f.setSize(500, 500);
        // Show frame
        f.setVisible(true);
    }
}
