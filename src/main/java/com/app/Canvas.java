package com.app;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.BorderFactory;
import javax.swing.JPanel;


public class Canvas extends JPanel {

    private Labyrinth labyrinth;

    public Canvas(Labyrinth labyrinth)
    {
        this.labyrinth = labyrinth;
        setBorder(BorderFactory.createLineBorder(Color.black)); // borderul panel-ului
    }

    public Labyrinth getLabyrinth() {
        return labyrinth;
    }

    //se executa atunci cand apelam repaint()
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);//apelez metoda paintComponent din clasa de baza

        g.drawString("This is the solved labyrinth!", 10, 20);

        final int labyrinthTopLeft = 44;
        final int squareSize = 20;

        if (getLabyrinth() != null) {
            labyrinth.drawMatrix(g, labyrinthTopLeft, squareSize);
        }
    }

}


